<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/produk', function () {
    return view('produk');
});

Route::get('/download', function () {
    return view('download');
});

Route::get('/spaj', function () {
    return view('spaj');
});

Route::get('/event', function () {
    return view('event');
});

Route::get('/ilustration', function () {
    return view('ilustration');
});
Route::get('/profile', function () {
    return view('profile');
});