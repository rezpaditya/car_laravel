@extends('layout')

@section('page_css')
@endsection

@section('content')
	<div class="container">
		<div class="row">

			<!-- Main Content -->

			<main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

				<div class="ui-block">
					
					<!-- News Feed Form  -->
					
					<div class="news-feed-form">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active inline-items" data-toggle="tab" href="{{ url('/') }}/car_asset/#home-1" role="tab" aria-expanded="true">
					
									<svg class="olymp-status-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-status-icon"></use></svg>
					
									<span>Status</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link inline-items" data-toggle="tab" href="{{ url('/') }}/car_asset/#profile-1" role="tab" aria-expanded="false">
					
									<svg class="olymp-multimedia-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-multimedia-icon"></use></svg>
					
									<span>Event</span>
								</a>
							</li>
					
							<li class="nav-item">
								<a class="nav-link inline-items" data-toggle="tab" href="{{ url('/') }}/car_asset/#blog" role="tab" aria-expanded="false">
									<svg class="olymp-blog-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-blog-icon"></use></svg>
					
									<span>Training</span>
								</a>
							</li>
						</ul>
					
						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
								<form>
									<div class="author-thumb">
										<img src="{{ url('/') }}/car_asset/img/author-page.jpg" alt="author">
									</div>
									<div class="form-group with-icon label-floating is-empty">
										<label class="control-label">Share what you are thinking here...</label>
										<textarea class="form-control" placeholder=""></textarea>
									</div>
									<div class="add-options-message">
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
											<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
										</a>
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
											<svg class="olymp-computer-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
										</a>
					
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
											<svg class="olymp-small-pin-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
										</a>
					
										<button class="btn btn-primary btn-md-2">Post Status</button>
										<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
					
									</div>
					
								</form>
							</div>
					
							<div class="tab-pane" id="profile-1" role="tabpanel" aria-expanded="true">
								<form>
									<div class="author-thumb">
										<img src="{{ url('/') }}/car_asset/img/author-page.jpg" alt="author">
									</div>
									<div class="form-group with-icon label-floating is-empty">
										<label class="control-label">Share what you are thinking here...</label>
										<textarea class="form-control" placeholder=""  ></textarea>
									</div>
									<div class="add-options-message">
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
											<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
										</a>
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
											<svg class="olymp-computer-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
										</a>
					
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
											<svg class="olymp-small-pin-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
										</a>
					
										<button class="btn btn-primary btn-md-2">Post Status</button>
										<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
					
									</div>
					
								</form>
							</div>
					
							<div class="tab-pane" id="blog" role="tabpanel" aria-expanded="true">
								<form>
									<div class="author-thumb">
										<img src="{{ url('/') }}/car_asset/img/author-page.jpg" alt="author">
									</div>
									<div class="form-group with-icon label-floating is-empty">
										<label class="control-label">Share what you are thinking here...</label>
										<textarea class="form-control" placeholder=""  ></textarea>
									</div>
									<div class="add-options-message">
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
											<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
										</a>
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
											<svg class="olymp-computer-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
										</a>
					
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
											<svg class="olymp-small-pin-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
										</a>
					
										<button class="btn btn-primary btn-md-2">Post Status</button>
										<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
					
									</div>
					
								</form>
							</div>
						</div>
					</div>
					
					<!-- ... end News Feed Form  -->			</div>
					<div id="newsfeed-items-grid">
						<div class="ui-block">
							<div class="row" style="padding:20px 20px 0px 20px">
								<div class="col-md-4">
									
									<div class="checkbox">
										<label>
											
											Feed Filter :				
										</label>
									</div>							
								</div>
								<div class="col-md-4">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											News & Articles
										</label>
									</div>							
								</div>
								<div class="col-md-4">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											Friends feeds
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div id="newsfeed-items-grid">
				<div class="ui-block">
						
						<article class="hentry post video">
						
							<div class="post__author author vcard inline-items">
								<img src="{{ url('/') }}/car_asset/img/avatar7-sm.jpg" alt="author">
						
								<div class="author-date">
									<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">Marina Valentine</a> shared a <a href="{{ url('/') }}/car_asset/#">link</a>
									<div class="post__date">
										<time class="published" datetime="2004-07-24T18:18">
											March 4 at 2:05pm
										</time>
									</div>
								</div>
						
								<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Edit Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Delete Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Turn Off Notifications</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Select as Featured</a>
										</li>
									</ul>
								</div>
						
							</div>
						
							<p>Halo <a href="{{ url('/') }}/car_asset/#">Cindi</a>, coba cek lagu baru dari Iron Maid deh. Tahun ini kalo tim mu achieve ku ajak nonton ini deh ! haha</p>
						
							<div class="post-video">
								<div class="video-thumb">
									<img src="{{ url('/') }}/car_asset/img/video-youtube1.jpg" alt="photo">
									<a href="{{ url('/') }}/car_asset/https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
										<svg class="olymp-play-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
									</a>
								</div>
						
								<div class="video-content">
									<a href="{{ url('/') }}/car_asset/#" class="h4 title">Iron Maid - ChillGroves</a>
									<p>Lorem ipsum dolor sit amet, consectetur ipisicing elit, sed do eiusmod tempor incididunt
										ut labore et dolore magna aliqua...
									</p>
									<a href="{{ url('/') }}/car_asset/#" class="link-site">YOUTUBE.COM</a>
								</div>
							</div>
						
							<div class="post-additional-info inline-items">
						
								<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
									<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
									<span>18</span>
								</a>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic9.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic11.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
								<div class="names-people-likes">
									<a href="{{ url('/') }}/car_asset/#">Jenny</a>, <a href="{{ url('/') }}/car_asset/#">Robert</a> and
									<br>18 more liked this
								</div>
						
								<div class="comments-shared">
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
						
										<span>0</span>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
						
										<span>16</span>
									</a>
								</div>
						
						
							</div>
						
							<div class="control-block-button post-control-button">
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-like-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-comments-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
								</a>
						
							</div>
						
						</article>
					</div>
						<div class="ui-block">
						
								<article class="hentry post video">
								
									<div class="post__author author vcard inline-items">
										<img src="{{ url('/') }}/car_asset/img/logo_symbol.png" alt="author">
								
										<div class="author-date">
											<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">CAR</a> shared an <a href="{{ url('/') }}/car_asset/#">News</a>
											<div class="post__date">
												<time class="published" datetime="2004-07-24T18:18">
													March 4 at 2:05pm
												</time>
											</div>
										</div>
								
										<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
											<ul class="more-dropdown">
												<li>
													<a href="{{ url('/') }}/car_asset/#">Edit Post</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Delete Post</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Turn Off Notifications</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Select as Featured</a>
												</li>
											</ul>
										</div>
								
									</div>
								
									<p>CAR Life Insurance kembali menyerahkan santunan klaim meninggal dunia kepada nasabah KPR Bank BCA Cabang Kudus.</p>
								
									<div class="post-video">
										<div class="video-thumb">
											<img src="http://www.car.co.id/media/342421/Klaim-BCA-2017.jpg" alt="photo">
										</div>
								
										<div class="video-content">
											<a href="{{ url('/') }}/car_asset/#" class="h4 title">Penyerahan Santunan Klaim Meninggal Dunia, Nasabah KPR Bank BCA
 </a>

											<p>Lorem ipsum dolor sit amet, consectetur ipisicing elit, sed do eiusmod tempor incididunt
												ut labore et dolore magna aliqua...
											</p>
											
											<hr>
											<button class="btn btn-success btn-sm">Read More</button>
										</div>
									</div>
								
									<div class="post-additional-info inline-items">
								
										<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
											<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
											<span>18</span>
										</a>
								
										<ul class="friends-harmonic">
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic9.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic11.jpg" alt="friend">
												</a>
											</li>
										</ul>
								
										<div class="names-people-likes">
											<a href="{{ url('/') }}/car_asset/#">Jenny</a>, <a href="{{ url('/') }}/car_asset/#">Robert</a> and
											<br>18 more liked this
										</div>
								
										<div class="comments-shared">
											<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
												<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
								
												<span>0</span>
											</a>
								
											<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
												<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
								
												<span>16</span>
											</a>
										</div>
								
								
									</div>
								
									<div class="control-block-button post-control-button">
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-like-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use></svg>
										</a>
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-comments-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
										</a>
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
										</a>
								
									</div>
								
								</article>
							</div>
						<div class="ui-block">
						
								<article class="hentry post video">
								
									<div class="post__author author vcard inline-items">
										<img src="{{ url('/') }}/car_asset/img/avatar3-sm.jpg" alt="author">
								
										<div class="author-date">
											<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">Lisa</a> shared an <a href="{{ url('/') }}/car_asset/#">event</a>
											<div class="post__date">
												<time class="published" datetime="2004-07-24T18:18">
													March 4 at 2:05pm
												</time>
											</div>
										</div>
								
										<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
											<ul class="more-dropdown">
												<li>
													<a href="{{ url('/') }}/car_asset/#">Edit Post</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Delete Post</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Turn Off Notifications</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">Select as Featured</a>
												</li>
											</ul>
										</div>
								
									</div>
								
									<p>Kita akan kedatangan 4 event nih di bulan July, daan berikut salah satu event berkelas tanggal 28 Agustus! Cekidot!</p>
								
									<div class="post-video">
										<div class="video-thumb">
											<img src="{{ url('/') }}/car_asset/img/blockchain.png" alt="photo">
										</div>
								
										<div class="video-content">
											<a href="{{ url('/') }}/car_asset/#" class="h4 title">Blockchain in Agency Training, Jakarta</a>

											<p>Lorem ipsum dolor sit amet, consectetur ipisicing elit, sed do eiusmod tempor incididunt
												ut labore et dolore magna aliqua...
											</p>
											<div class="place inline-items">
												<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
												<span>Balai Sarbini, Jakarta</span>
											</div>
											<br>
											<div class="place inline-items">
												<svg class="olymp-small-calendar-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-small-calendar-icon"></use></svg>
												<span>28th August</span>
											</div>
											<hr>
											<button class="btn btn-success btn-sm">Add to Calendar</button>
										</div>
									</div>
								
									<div class="post-additional-info inline-items">
								
										<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
											<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
											<span>18</span>
										</a>
								
										<ul class="friends-harmonic">
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic9.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
												</a>
											</li>
											<li>
												<a href="{{ url('/') }}/car_asset/#">
													<img src="{{ url('/') }}/car_asset/img/friend-harmonic11.jpg" alt="friend">
												</a>
											</li>
										</ul>
								
										<div class="names-people-likes">
											<a href="{{ url('/') }}/car_asset/#">Jenny</a>, <a href="{{ url('/') }}/car_asset/#">Robert</a> and
											<br>18 more liked this
										</div>
								
										<div class="comments-shared">
											<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
												<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
								
												<span>0</span>
											</a>
								
											<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
												<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
								
												<span>16</span>
											</a>
										</div>
								
								
									</div>
								
									<div class="control-block-button post-control-button">
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-like-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use></svg>
										</a>
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-comments-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
										</a>
								
										<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
											<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
										</a>
								
									</div>
								
								</article>
							</div>

					

					<div class="ui-block">

						
						<article class="hentry post">
						
							<div class="post__author author vcard inline-items">
								<img src="{{ url('/') }}/car_asset/img/avatar10-sm.jpg" alt="author">
						
								<div class="author-date">
									<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">Elaine Dreyfuss</a>
									<div class="post__date">
										<time class="published" datetime="2004-07-24T18:18">
											9 hours ago
										</time>
									</div>
								</div>
						
								<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Edit Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Delete Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Turn Off Notifications</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Select as Featured</a>
										</li>
									</ul>
								</div>
						
							</div>
						
							<p>Sepertinya ada yang agak aneh tadi siang di kantor, entah karena pasca lebaran atau apa ya kok berasa sepi dan tenang gitu...
							</p>
						
							<div class="post-additional-info inline-items">
						
								<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
									<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
									<span>24</span>
								</a>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic9.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic11.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
								<div class="names-people-likes">
									<a href="{{ url('/') }}/car_asset/#">You</a>, <a href="{{ url('/') }}/car_asset/#">Elaine</a> and
									<br>22 more liked this
								</div>
						
						
								<div class="comments-shared">
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
										<span>17</span>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
										<span>24</span>
									</a>
								</div>
						
						
							</div>
						
							<div class="control-block-button post-control-button">
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-like-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-comments-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
								</a>
						
							</div>
						
						</article>
						
						<!-- Comments -->
						
						<ul class="comments-list">
							<li class="comment-item">
								<div class="post__author author vcard inline-items">
									<img src="{{ url('/') }}/car_asset/img/author-page.jpg" alt="author">
						
									<div class="author-date">
										<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/02-ProfilePage.html">James Spiegel</a>
										<div class="post__date">
											<time class="published" datetime="2004-07-24T18:18">
												38 mins ago
											</time>
										</div>
									</div>
						
									<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
						
								</div>
						
								<p>Mungkin mereka ga tahu...</p>
						
								<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
									<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
									<span>3</span>
								</a>
								<a href="{{ url('/') }}/car_asset/#" class="reply">Reply</a>
							</li>
							<li class="comment-item">
								<div class="post__author author vcard inline-items">
									<img src="{{ url('/') }}/car_asset/img/avatar1-sm.jpg" alt="author">
						
									<div class="author-date">
										<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">Mathilda Brinker</a>
										<div class="post__date">
											<time class="published" datetime="2004-07-24T18:18">
												1 hour ago
											</time>
										</div>
									</div>
						
									<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
						
								</div>
						
								<p>Haha benar juga, saya juga pernah seperti itu.
								</p>
						
								<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
									<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
									<span>8</span>
								</a>
								<a href="{{ url('/') }}/car_asset/#" class="reply">Reply</a>
							</li>
						</ul>
						
						<!-- ... end Comments -->

						<a href="{{ url('/') }}/car_asset/#" class="more-comments">View more comments <span>+</span></a>

						
						<!-- Comment Form  -->
						
						<form class="comment-form inline-items">
						
							<div class="post__author author vcard inline-items">
								<img src="{{ url('/') }}/car_asset/img/author-page.jpg" alt="author">
						
								<div class="form-group with-icon-right ">
									<textarea class="form-control" placeholder=""></textarea>
									<div class="add-options-message">
										<a href="{{ url('/') }}/car_asset/#" class="options-message" data-toggle="modal" data-target="#update-header-photo">
											<svg class="olymp-camera-icon">
												<use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
											</svg>
										</a>
									</div>
								</div>
							</div>
						
							<button class="btn btn-md-2 btn-primary">Post Comment</button>
						
							<button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>
						
						</form>
						
						<!-- ... end Comment Form  -->
					</div>

					<div class="ui-block">
						
						<article class="hentry post has-post-thumbnail">
						
							<div class="post__author author vcard inline-items">
								<img src="{{ url('/') }}/car_asset/img/avatar5-sm.jpg" alt="author">
						
								<div class="author-date">
									<a class="h6 post__author-name fn" href="{{ url('/') }}/car_asset/#">Green Goo Rock</a>
									<div class="post__date">
										<time class="published" datetime="2004-07-24T18:18">
											March 8 at 6:42pm
										</time>
									</div>
								</div>
						
								<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Edit Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Delete Post</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Turn Off Notifications</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Select as Featured</a>
										</li>
									</ul>
								</div>
						
							</div>
						
							<p>Sunday morning in <a href="{{ url('/') }}/car_asset/#">River Valley Street, Singapore</a> so chillin!
								Liburan tahun depan Employee Gathering ke sini seru kali!
							</p>
						
							<div class="post-thumb">
								<img src="{{ url('/') }}/car_asset/img/img-exp.jpg" alt="photo">
							</div>
						
							<div class="post-additional-info inline-items">
						
								<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
									<svg class="olymp-heart-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
									<span>49</span>
								</a>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic9.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic11.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
								<div class="names-people-likes">
									<a href="{{ url('/') }}/car_asset/#">Jimmy</a>, <a href="{{ url('/') }}/car_asset/#">Andrea</a> and
									<br>47 more liked this
								</div>
						
						
								<div class="comments-shared">
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
										<span>264</span>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
										<span>37</span>
									</a>
								</div>
						
						
							</div>
						
							<div class="control-block-button post-control-button">
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-like-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-comments-post-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
								</a>
						
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control">
									<svg class="olymp-share-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-share-icon"></use></svg>
								</a>
						
							</div>
						
						</article>
					</div>

				</div>

				<a id="load-more-button" href="{{ url('/') }}/car_asset/#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

			</main>

			<!-- ... end Main Content -->


			<!-- Left Sidebar -->

			<aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
				<div class="ui-block">
					
					<!-- W-Calendar -->
					
					<div class="w-calendar calendar-container">
						<div class="calendar">
							<header>
								<h6 class="month">March 2017</h6>
								<a class="calendar-btn-prev fas fa-angle-left" href="{{ url('/') }}/car_asset/#"></a>
								<a class="calendar-btn-next fas fa-angle-right" href="{{ url('/') }}/car_asset/#"></a>
							</header>
							<table>
								<thead>
								<tr><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td><td>San</td></tr>
								</thead>
								<tbody>
								<tr>
									<td data-month="12" data-day="1">1</td>
									<td data-month="12" data-day="2" class="event-uncomplited event-complited">
										2
									</td>
									<td data-month="12" data-day="3">3</td>
									<td data-month="12" data-day="4">4</td>
									<td data-month="12" data-day="5">5</td>
									<td data-month="12" data-day="6">6</td>
									<td data-month="12" data-day="7">7</td>
								</tr>
								<tr>
									<td data-month="12" data-day="8">8</td>
									<td data-month="12" data-day="9">9</td>
									<td data-month="12" data-day="10" class="event-complited">10</td>
									<td data-month="12" data-day="11">11</td>
									<td data-month="12" data-day="12">12</td>
									<td data-month="12" data-day="13">13</td>
									<td data-month="12" data-day="14">14</td>
								</tr>
								<tr>
									<td data-month="12" data-day="15" class="event-complited-2">15</td>
									<td data-month="12" data-day="16">16</td>
									<td data-month="12" data-day="17">17</td>
									<td data-month="12" data-day="18">18</td>
									<td data-month="12" data-day="19">19</td>
									<td data-month="12" data-day="20">20</td>
									<td data-month="12" data-day="21">21</td>
								</tr>
								<tr>
									<td data-month="12" data-day="22">22</td>
									<td data-month="12" data-day="23">23</td>
									<td data-month="12" data-day="24">24</td>
									<td data-month="12" data-day="25">25</td>
									<td data-month="12" data-day="26">26</td>
									<td data-month="12" data-day="27">27</td>
									<td data-month="12" data-day="28" class="event-uncomplited">28</td>
								</tr>
								<tr>
									<td data-month="12" data-day="29">29</td>
									<td data-month="12" data-day="30">30</td>
									<td data-month="12" data-day="31">31</td>
								</tr>
								</tbody>
							</table>
							<div class="list">
					
								<div id="accordion-1" role="tablist" aria-multiselectable="true" class="day-event" data-month="12" data-day="2">
									<div class="ui-block-title ui-block-title-small">
										<h6 class="title">TODAY’S EVENTS</h6>
									</div>
									<div class="card">
										<div class="card-header" role="tab" id="headingOne-1">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">9:00am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
													Breakfast at the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseOne-1" class="collapse" role="tabpanel" >
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
											<div class="place inline-items">
												<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
												<span>Daydreamz Agency</span>
											</div>
					
											<ul class="friends-harmonic inline-items">
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic2.jpg" alt="friend">
													</a>
												</li>
												<li class="with-text">
													Will Assist
												</li>
											</ul>
										</div>
									</div>
					
									<div class="card">
										<div class="card-header" role="tab" id="headingTwo-1">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">9:00am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseTwo-1" aria-expanded="true" aria-controls="collapseTwo-1">
													Send the new “Olympus” project files to the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseTwo-1" class="collapse" role="tabpanel">
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
										</div>
					
									</div>
					
									<div class="card">
										<div class="card-header" role="tab" id="headingThree-1">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">6:30am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#" aria-expanded="false">
													Take Querty to the Veterinarian
												</a>
											</h5>
										</div>
										<div class="place inline-items">
											<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
											<span>Daydreamz Agency</span>
										</div>
									</div>
					
									<a href="{{ url('/') }}/car_asset/#" class="check-all">Check all your Events</a>
								</div>
					
								<div id="accordion-2" role="tablist" aria-multiselectable="true" class="day-event" data-month="12" data-day="10">
									<div class="ui-block-title ui-block-title-small">
										<h6 class="title">TODAY’S EVENTS</h6>
									</div>
									<div class="card">
										<div class="card-header" role="tab" id="headingOne-2">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">9:00am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseOne-2" aria-expanded="true" aria-controls="collapseOne-2">
													Breakfast at the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseOne-2" class="collapse" role="tabpanel">
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
											<div class="place inline-items">
												<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
												<span>Daydreamz Agency</span>
											</div>
					
											<ul class="friends-harmonic inline-items">
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic2.jpg" alt="friend">
													</a>
												</li>
												<li class="with-text">
													Will Assist
												</li>
											</ul>
										</div>
					
									</div>
					
									<a href="{{ url('/') }}/car_asset/#" class="check-all">Check all your Events</a>
								</div>
					
								<div id="accordion-3" role="tablist" aria-multiselectable="true" class="day-event" data-month="12" data-day="15">
									<div class="ui-block-title ui-block-title-small">
										<h6 class="title">TODAY’S EVENTS</h6>
									</div>
									<div class="card">
										<div class="card-header" role="tab" id="headingOne-3">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">9:00am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseOne-3" aria-expanded="true" aria-controls="collapseOne-3">
													Breakfast at the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseOne-3" class="collapse" role="tabpanel">
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
					
											<div class="place inline-items">
												<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
												<span>Daydreamz Agency</span>
											</div>
					
											<ul class="friends-harmonic inline-items">
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic2.jpg" alt="friend">
													</a>
												</li>
												<li class="with-text">
													Will Assist
												</li>
											</ul>
										</div>
					
									</div>
					
									<div class="card">
										<div class="card-header" role="tab" id="headingTwo-3">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">12:00pm</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseTwo-3" aria-expanded="true" aria-controls="collapseTwo-3">
													Send the new “Olympus” project files to the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseTwo-3" class="collapse" role="tabpanel" >
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
										</div>
					
									</div>
					
									<div class="card">
										<div class="card-header" role="tab" id="headingThree-3">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">6:30pm</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#" aria-expanded="false">
													Take Querty to the Veterinarian
												</a>
											</h5>
										</div>
										<div class="place inline-items">
											<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
											<span>Daydreamz Agency</span>
										</div>
									</div>
					
									<a href="{{ url('/') }}/car_asset/#" class="check-all">Check all your Events</a>
								</div>
					
								<div id="accordion-4" role="tablist" aria-multiselectable="true" class="day-event" data-month="12" data-day="28">
									<div class="ui-block-title ui-block-title-small">
										<h6 class="title">TODAY’S EVENTS</h6>
									</div>
									<div class="card">
										<div class="card-header" role="tab" id="headingOne-4">
											<div class="event-time">
												<span class="circle"></span>
												<time datetime="2004-07-24T18:18">9:00am</time>
												<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
											</div>
											<h5 class="mb-0">
												<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseOne-4" aria-expanded="true" aria-controls="collapseOne-4">
													Breakfast at the Agency<svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
												</a>
											</h5>
										</div>
					
										<div id="collapseOne-4" class="collapse" role="tabpanel" aria-labelledby="headingOne-4">
											<div class="card-body">
												Hi Guys! I propose to go a litle earlier at the agency to have breakfast and talk a little more about the new design project we have been working on. Cheers!
											</div>
											<div class="place inline-items">
												<svg class="olymp-add-a-place-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-add-a-place-icon"></use></svg>
												<span>Daydreamz Agency</span>
											</div>
					
											<ul class="friends-harmonic inline-items">
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
													</a>
												</li>
												<li>
													<a href="{{ url('/') }}/car_asset/#">
														<img src="{{ url('/') }}/car_asset/img/friend-harmonic2.jpg" alt="friend">
													</a>
												</li>
												<li class="with-text">
													Will Assist
												</li>
											</ul>
										</div>
					
									</div>
					
									<a href="{{ url('/') }}/car_asset/#" class="check-all">Check all your Events</a>
								</div>
					
							</div>
						</div>
					</div>
					
					<!-- ... end W-Calendar -->			</div>

				<div class="ui-block">
					<div class="ui-block-title">
						<h6 class="title">Your Achivement</h6>
						<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
					</div>

					<!-- W-Friend-Pages-Added -->
					
					<ul class="widget w-friend-pages-added notification-list friend-requests">
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar41-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">SPAJ Master</a>
								<span class="chat-message-item">18/20 SPAJ in a month </span>
							</div>
					
						</li>
					
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar42-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Gold Star</a>
								<span class="chat-message-item">120/200 Sales per quarter</span>
							</div>				
						</li>
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar42-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Platinum Star</a>
								<span class="chat-message-item">120/300 Sales per quarter</span>
							</div>				
						</li>
					
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar43-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Hot Leader</a>
								<span class="chat-message-item">12/20 Downline Achieve </span>
							</div>
						</li>
					
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar44-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Hotter Leader</a>
								<span class="chat-message-item">12/50 Downline Achieve</span>
							</div>
					
						</li>
					
						<li class="inline-items">
							<div class="author-thumb">							
								<img src="{{ url('/') }}/car_asset/img/avatar46-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Crimson Agency</a>
								<span class="chat-message-item">8/30 Family Pack</span>
							</div>
						</li>
					
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar45-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Agent Angel</a>
								<span class="chat-message-item">5/20 Badges in a year</span>
							</div>
						</li>
					</ul>
					
					<!-- .. end W-Friend-Pages-Added -->
				</div>
			</aside>

			<!-- ... end Left Sidebar -->


			<!-- Right Sidebar -->

			<aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

				<div class="ui-block">
					
					<!-- W-Birthsday-Alert -->
					
					<div class="widget w-birthday-alert">
						<div class="icons-block">
							<svg class="olymp-day-calendar-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-day-calendar-icon"></use></svg>
							<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"></svg><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
						</div>
					
						<div class="content">
							<span>Tomorrow is</span>
							<a href="{{ url('/') }}/car_asset/#" class="h4 title">Global Marketing Meetup</a>
							<p>Siapkan dirimu untuk event terbesar tahun ini!</p>
						</div>
					</div>
					
					<!-- ... end W-Birthsday-Alert -->			</div>

				<div class="ui-block">

					<div class="ui-block-title">
						<h6 class="title">Activity Feed</h6>
						<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
					</div>

					
					<!-- W-Activity-Feed -->
					
					<ul class="widget w-activity-feed notification-list">
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar49-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Marina Polson</a> commented on Jason Mark’s <a href="{{ url('/') }}/car_asset/#" class="notification-link">photo.</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 mins ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar9-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Jake Parker </a> receive new Badges <a href="{{ url('/') }}/car_asset/#" class="notification-link">SPAJ Master.</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">5 mins ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar50-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Mary Jane Stark </a> join event <a href="{{ url('/') }}/car_asset/#" class="notification-link">Sales for Profesional.</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">12 mins ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar51-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Nicholas Grissom </a> updated his profile <a href="{{ url('/') }}/car_asset/#" class="notification-link">photo</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
							</div>
						</li>
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar48-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Marina Valentine </a> commented on Chris Greyson’s <a href="{{ url('/') }}/car_asset/#" class="notification-link">status update</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar52-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Green Goo Rock </a> posted a <a href="{{ url('/') }}/car_asset/#" class="notification-link">status update</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
							</div>
						</li>
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar10-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Elaine Dreyfuss  </a> liked your <a href="{{ url('/') }}/car_asset/#" class="notification-link">blog post</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar10-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Elaine Dreyfuss  </a> commented on your <a href="{{ url('/') }}/car_asset/#" class="notification-link">blog post</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
							</div>
						</li>
					
						<li>
							<div class="author-thumb">
								<img src="{{ url('/') }}/car_asset/img/avatar53-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<a href="{{ url('/') }}/car_asset/#" class="h6 notification-friend">Bruce Peterson </a> changed his <a href="{{ url('/') }}/car_asset/#" class="notification-link">profile picture</a>.
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">15 hours ago</time></span>
							</div>
						</li>
					
					</ul>
					
					<!-- .. end W-Activity-Feed -->
				</div>

			</aside>

			<!-- ... end Right Sidebar -->

		</div>
	</div>
@endsection

@section('page_script')
	<script src="{{ url('/') }}/car_asset/js/base-init.js"></script>
	<script defer src="{{ url('/') }}/car_asset/fonts/fontawesome-all.js"></script>
	<script src="{{ url('/') }}/car_asset/Bootstrap/dist/js/bootstrap.bundle.js"></script>
@endsection