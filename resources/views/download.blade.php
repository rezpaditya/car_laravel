@extends('layout')

@section('content')
	<div class="container">
			<div class="form-group with-icon label-floating is-empty">
					<label class="control-label">Search File</label>
					<input class="form-control" type="text">
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
		<div class="row">

			<div class="col col-lg-3 col-md-3 col-sm-6 col-6">
					<div class="ui-block video-item">
						<div>
							<img src="{{ url('/') }}/car_asset/img/video10.jpg" alt="photo">
						</div>
					
						<div class="ui-block-content video-content">
							<a href="{{ url('/') }}/car_asset/#" class="h6 title" data-toggle="modal" data-target="#private-event">File Detail 1</a>
							<br>
							<div class="post-additional-info inline-items">
								
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<span>18 Downloads</span>
									</a>
									<div class="names-people-likes">
									</div>
									<div class="names-people-likes">
										<a href="{{ url('/') }}/car_asset/#" style="margin-right: 15px"><i class="fa fa-share"></i></a>
										<a href="{{ url('/') }}/car_asset/#"><i class="fa fa-download"></i></a>
									</div>
								</div>
						</div>
					</div>
					
		
			</div>
			
			<div class="col col-lg-3 col-md-3 col-sm-6 col-6">
					<div class="ui-block video-item">
						<div>
							<img src="{{ url('/') }}/car_asset/img/video10.jpg" alt="photo">
						</div>
					
						<div class="ui-block-content video-content">
							<a href="{{ url('/') }}/car_asset/#" class="h6 title">File Example 2</a>
							<br>
							<div class="post-additional-info inline-items">
								
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<span>33 Downloads</span>
									</a>
									<div class="names-people-likes">
									</div>
									<div class="names-people-likes">
										<a href="{{ url('/') }}/car_asset/#" style="margin-right: 15px"><i class="fa fa-share"></i></a>
										<a href="{{ url('/') }}/car_asset/#"><i class="fa fa-download"></i></a>
									</div>
								</div>
						</div>
					</div>
					
		
			</div>
			<div class="col col-lg-3 col-md-3 col-sm-6 col-6">
					<div class="ui-block video-item">
						<div>
							<img src="{{ url('/') }}/car_asset/img/video10.jpg" alt="photo">
						</div>
					
						<div class="ui-block-content video-content">
							<a href="{{ url('/') }}/car_asset/#" class="h6 title">File Example 3</a>
							<br>
							<div class="post-additional-info inline-items">
								
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<span>25 Downloads</span>
									</a>
									<div class="names-people-likes">
									</div>
									<div class="names-people-likes">
										<a href="{{ url('/') }}/car_asset/#" style="margin-right: 15px"><i class="fa fa-share"></i></a>
										<a href="{{ url('/') }}/car_asset/#"><i class="fa fa-download"></i></a>
									</div>
								</div>
						</div>
					</div>
					
		
			</div>
			<div class="col col-lg-3 col-md-3 col-sm-6 col-6">
					<div class="ui-block video-item">
						<div>
							<img src="{{ url('/') }}/car_asset/img/video10.jpg" alt="photo">
						</div>
					
						<div class="ui-block-content video-content">
							<a href="{{ url('/') }}/car_asset/#" class="h6 title">File Example 4</a>
							<br>
							<div class="post-additional-info inline-items">
								
									<a href="{{ url('/') }}/car_asset/#" class="post-add-icon inline-items">
										<span>3 Downloads</span>
									</a>
									<div class="names-people-likes">
									</div>
									<div class="names-people-likes">
										<a href="{{ url('/') }}/car_asset/#" style="margin-right: 15px"><i class="fa fa-share"></i></a>
										<a href="{{ url('/') }}/car_asset/#"><i class="fa fa-download"></i></a>
									</div>
								</div>
						</div>
					</div>
					
		
			</div>
		</div>

		<nav aria-label="Page navigation">
				<ul class="pagination justify-content-center">
					<li class="page-item disabled">
						<a class="page-link" href="{{ url('/') }}/car_asset/#" tabindex="-1">Previous</a>
					</li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">2</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">3</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">...</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">12</a></li>
					<li class="page-item">
						<a class="page-link" href="{{ url('/') }}/car_asset/#">Next</a>
					</li>
				</ul>
			</nav>
	</div>
@endsection

@section('page_script')
	<script src="{{ url('/') }}/car_asset/js/base-init.js"></script>
	<script defer src="{{ url('/') }}/car_asset/fonts/fontawesome-all.js"></script>
	<script src="{{ url('/') }}/car_asset/Bootstrap/dist/js/bootstrap.bundle.js"></script>
@endsection