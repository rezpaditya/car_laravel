@extends('layout')

@section('page_css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" />
@endsection

@section('content')
	<div class="container">
	<div class="row">

		<!-- Main Content -->

		<main class="col col-xl-12 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
			<!-- BAGIAN TENGAH HALAMAN -->
			<!-- start filter -->
			<div class="ui-block">
					<a data-toggle="collapse" data-parent="#accordion" href="{{ url('/') }}/car_asset/#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
				<div class="ui-block-title">
					<h6 class="mb-0">
						Ilustrasi Proposal</h6>
				</div></a>
				
				<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" style="">
					<div class="ui-block-content">

						<img src="{{ url('/') }}/car_asset/img/capture.png">

						<div class="row">
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="crumina-module crumina-heading with-title-decoration">
									<h5 class="heading-title">Data Agen</h5>
								</div>
							</div>	
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Nama Agen" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Kode Agen" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="crumina-module crumina-heading with-title-decoration">
									<h5 class="heading-title">Data Nasabah</h5>
								</div>
							</div>	
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Nama Tertanggung" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Nama Pemegang Polis" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">Tanggal Lahir</label>
									<input name="datetimepicker" value="10/24/1984" />
									<span class="input-group-addon">
										<svg class="olymp-month-calendar-icon icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
									</span>
								</div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">Tanggal Lahir</label>
									<input name="datetimepicker" value="10/24/1984" />
									<span class="input-group-addon">
										<svg class="olymp-month-calendar-icon icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
									</span>
								</div>
							</div>
							<div class="col col-lg-6 col-md-4 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label"></label>
									<label class="control-label">Jenis Kelamin</label>
									<select class="selectpicker form-control">
											<option value="All">Pria</option>
											<option value="1">Wanita</option>
									</select>
								</div>
							</div>
							<div class="col col-lg-6 col-md-4 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label"></label>
									<label class="control-label">Jenis Kelamin</label>
									<select class="selectpicker form-control">
										<option value="All">Pria</option>
										<option value="1">Wanita</option>
									</select>
								</div>
							</div>
							<div class="col col-lg-6 col-md-4 col-sm-12 col-12">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes">
										Mengasuransikan Diri Sendiri
									</label>
								</div>
							</div>
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="crumina-module crumina-heading with-title-decoration">
									<h5 class="heading-title">Data Investasi</h5>
								</div>
							</div>	
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label">Produk Unit Link</label>
									<select class="selectpicker form-control">
										<option value="MA">CARLink Pro</option>
										<option value="FE">Option 2</option>
									</select>
								</div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Premi Utama" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label">Masa Bayar</label>
									<select class="selectpicker form-control">
										<option value="MA">Sekaligus</option>
										<option value="FE">Option 2</option>
									</select>
								</div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Top Up Regular" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label">Mata Uang</label>
									<select class="selectpicker form-control">
										<option value="MA">Rupiah</option>
										<option value="FE">Dollar</option>
									</select>
								</div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12"></div>
							<div class="col col-lg-6 col-md-4 col-sm-12 col-12">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes" checked>
										Cara Bayar <b>Tahunan</b>
									</label>
								</div>
							</div>

							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="crumina-module crumina-heading with-title-decoration">
									<h5 class="heading-title">Alokasi Investasi</h5>
								</div>
							</div>	
							<div class="col col-lg-4 col-md-4 col-sm-12 col-12">
								<h6>Jenis Investasi</h6>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes">
										Safe
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes">
										Fixed
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes">
										Mixed
									</label>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes">
										Flexy
									</label>
								</div>
							</div>

							<div class="col col-lg-8 col-md-8 col-sm-12 col-12"></div>
							<div class="col col-lg-4 col-md-4 col-sm-12 col-12">
								<button class="btn btn-info btn-sm">Previous</button>
								<button class="btn btn-green btn-sm">Next</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end filter -->

		</main>

		<!-- ... end Main Content -->

		<!-- Right Sidebar -->

		<!-- <aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12"> -->
			<!-- BAGIAN KANAN HALAMAN -->
		<!-- </aside> -->

		<!-- ... end Right Sidebar -->

	</div>
</div>
@endsection

@section('page_script')

<script src="{{ url('/') }}/car_asset/js/base-init.js"></script>
<script defer src="{{ url('/') }}/car_asset/fonts/fontawesome-all.js"></script>

<script src="{{ url('/') }}/car_asset/Bootstrap/dist/js/bootstrap.bundle.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script>
	var $treeview = $(function () { $('#jstree_demo_div').jstree(); });
	
	$('#jstree_demo_div').on('loaded.jstree', function() {
		$('#jstree_demo_div').jstree('open_all');
	});

	$('#jstree_demo_div').on("changed.jstree", function (e, data) {
		console.log(data.selected);
	});

	$('button').on('click', function () {
	$('#jstree').jstree(true).select_node('child_node_1');
	$('#jstree').jstree('select_node', 'child_node_1');
	$.jstree.reference('#jstree').select_node('child_node_1');
	});

	$(document).ready(function() {
		$('#1').DataTable();
	} );
</script>
@endsection