@extends('layout')

@section('content')
	<div class="container">
			<div class="form-group with-icon label-floating is-empty">
					<label class="control-label">Search Product</label>
					<input class="form-control" type="text">
					<i class="fa fa-search" aria-hidden="true"></i>
				</div>
		<div class="row">

			

			<div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
				
				<!-- Friend Item -->
				
				<div class="friend-item friend-groups create-group" data-mh="friend-groups-item">
				
					<a href="{{ url('/') }}/car_asset/#" class="  full-block" data-toggle="modal" data-target="#create-friend-group-1"></a>
					<div class="content">
				
						<a href="{{ url('/') }}/car_asset/#" class="  btn btn-control bg-blue" data-toggle="modal" data-target="#create-friend-group-1">
							<svg class="olymp-plus-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>
						</a>
				
						<div class="author-content">
							<a href="{{ url('/') }}/car_asset/#" class="h5 author-name">New Product</a>
							<div class="country">Create new product</div>
						</div>
				
					</div>
				
				</div>
				
				<!-- ... end Friend Item -->		</div>

			<div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
				<div class="ui-block" data-mh="friend-groups-item">
					
					<!-- Friend Item -->
					
					<div class="friend-item friend-groups">
					
						<div class="friend-item-content">
					
							<div class="more">
								<svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="{{ url('/') }}/car_asset/#">Simulation</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">Download Files</a>
									</li>
								</ul>
							</div>
							<div class="friend-avatar">
								<div class="author-thumb">
									<img src="http://www.car.co.id/ImageGen.ashx?image=/media/135442/Product-Individu.jpg" alt="Olympus">
								</div>
								<div class="author-content">
									<a href="{{ url('/') }}/car_asset/#" class="h5 author-name">Individu</a>
									<div class="country">Produk Asuransi untuk Melindungi Pribadi dan Keluarga</div>
									<br>
									<div class="country">Favorited by 6 Collegueas</div>
								</div>
							</div>
					
							<ul class="friends-harmonic">
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/friend-harmonic2.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="{{ url('/') }}/car_asset/#">
										<img src="{{ url('/') }}/car_asset/img/avatar30-sm.jpg" alt="author">
									</a>
								</li>
							</ul>
					
					
							<div class="control-block-button">
								<a href="{{ url('/') }}/car_asset/#" class="  btn btn-control bg-blue" data-toggle="modal" data-target="#create-friend-group-add-friends">
									<svg class="olymp-happy-faces-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-happy-faces-icon"></use></svg>
								</a>
					
								<a href="{{ url('/') }}/car_asset/#" class="btn btn-control btn-grey-lighter">
									<svg class="olymp-settings-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-settings-icon"></use></svg>
								</a>
					
							</div>
						</div>
					</div>
					
					<!-- ... end Friend Item -->			</div>
			</div>

			<div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
					<div class="ui-block" data-mh="friend-groups-item">
						
						<!-- Friend Item -->
						
						<div class="friend-item friend-groups">
						
							<div class="friend-item-content">
						
								<div class="more">
									<svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Simulation</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Download Files</a>
										</li>
									</ul>
								</div>
								<div class="friend-avatar">
									<div class="author-thumb">
										<img src="http://www.car.co.id//ImageGen.ashx?image=/media/135460/Product-Group.jpg" alt="Olympus">
									</div>
									<div class="author-content">
										<a href="{{ url('/') }}/car_asset/#" class="h5 author-name">Group Product</a>
										<div class="country">Produk Asuransi untuk Memberikan Kenyamanan kepada Karyawan dan Anggota Badan Usaha/Perkumpulan</div>
										<br>
										<div class="country">Favorited by 2 Collegueas</div>
									</div>
								</div>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
						
								<div class="control-block-button">
									<a href="{{ url('/') }}/car_asset/#" class="  btn btn-control bg-blue" data-toggle="modal" data-target="#create-friend-group-add-friends">
										<svg class="olymp-happy-faces-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-happy-faces-icon"></use></svg>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="btn btn-control btn-grey-lighter">
										<svg class="olymp-settings-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-settings-icon"></use></svg>
									</a>
						
								</div>
							</div>
						</div>
						
						<!-- ... end Friend Item -->			</div>
				</div>

			<div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
					<div class="ui-block" data-mh="friend-groups-item">
						
						<!-- Friend Item -->
						
						<div class="friend-item friend-groups">
						
							<div class="friend-item-content">
						
								<div class="more">
									<svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Simulation</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Download Files</a>
										</li>
									</ul>
								</div>
								<div class="friend-avatar">
									<div class="author-thumb">
										<img src="http://www.car.co.id//ImageGen.ashx?image=/media/135461/Product-Bancassurance2.jpg" alt="Olympus">
									</div>
									<div class="author-content">
										<a href="{{ url('/') }}/car_asset/#" class="h5 author-name">Bancassurance</a>
										<div class="country">Memberikan Kepastian Pengembalian Kredit dan Nilai Tambah Produk Perbankan - Jasa Keuangan</div>
										<br>
										<div class="country">Favorited by 4 Collegueas</div>
									</div>
								</div>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic10.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic7.jpg" alt="friend">
										</a>
									</li>
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic8.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
						
								<div class="control-block-button">
									<a href="{{ url('/') }}/car_asset/#" class="  btn btn-control bg-blue" data-toggle="modal" data-target="#create-friend-group-add-friends">
										<svg class="olymp-happy-faces-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-happy-faces-icon"></use></svg>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="btn btn-control btn-grey-lighter">
										<svg class="olymp-settings-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-settings-icon"></use></svg>
									</a>
						
								</div>
							</div>
						</div>
						
						<!-- ... end Friend Item -->			</div>
				</div>

			<div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
					<div class="ui-block" data-mh="friend-groups-item">
						
						<!-- Friend Item -->
						
						<div class="friend-item friend-groups">
						
							<div class="friend-item-content">
						
								<div class="more">
									<svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
									<ul class="more-dropdown">
										<li>
											<a href="{{ url('/') }}/car_asset/#">Simulation</a>
										</li>
										<li>
											<a href="{{ url('/') }}/car_asset/#">Download Files</a>
										</li>
									</ul>
								</div>
								<div class="friend-avatar">
									<div class="author-thumb">
										<img src="{{ url('/') }}/car_asset/img/logo.png" alt="Olympus">
									</div>
									<div class="author-content">
										<a href="{{ url('/') }}/car_asset/#" class="h5 author-name">Product D</a>
										<div class="country">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
										<br>
										<div class="country">Favorited by 1 Colleguea</div>
									</div>
								</div>
						
								<ul class="friends-harmonic">
									<li>
										<a href="{{ url('/') }}/car_asset/#">
											<img src="{{ url('/') }}/car_asset/img/friend-harmonic5.jpg" alt="friend">
										</a>
									</li>
								</ul>
						
						
								<div class="control-block-button">
									<a href="{{ url('/') }}/car_asset/#" class="  btn btn-control bg-blue" data-toggle="modal" data-target="#create-friend-group-add-friends">
										<svg class="olymp-happy-faces-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-happy-faces-icon"></use></svg>
									</a>
						
									<a href="{{ url('/') }}/car_asset/#" class="btn btn-control btn-grey-lighter">
										<svg class="olymp-settings-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-settings-icon"></use></svg>
									</a>
						
								</div>
							</div>
						</div>
						
						<!-- ... end Friend Item -->			</div>
				</div>
			
			
		</div>

		<nav aria-label="Page navigation">
				<ul class="pagination justify-content-center">
					<li class="page-item disabled">
						<a class="page-link" href="{{ url('/') }}/car_asset/#" tabindex="-1">Previous</a>
					</li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">2</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">3</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">...</a></li>
					<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">12</a></li>
					<li class="page-item">
						<a class="page-link" href="{{ url('/') }}/car_asset/#">Next</a>
					</li>
				</ul>
			</nav>
	</div>
@endsection

@section('page_script')
	<script src="{{ url('/') }}/car_asset/js/base-init.js"></script>
	<script defer src="{{ url('/') }}/car_asset/fonts/fontawesome-all.js"></script>
	<script src="{{ url('/') }}/car_asset/Bootstrap/dist/js/bootstrap.bundle.js"></script>
@endsection