@extends('layout')

@section('page_css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" />
	<style>
		.jstree-default  .jstree-themeicon-custom {
			width: 15px;
		}
	</style>
@endsection

@section('content')
<div class="container">
	<div class="row">

		<!-- Main Content -->

		<main class="col col-xl-8 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
			<!-- BAGIAN TENGAH HALAMAN -->
			<!-- start filter -->
			<div class="ui-block">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
				<div class="ui-block-title">
					<h6 class="mb-0">
						Filter</h6>
				</div></a>
				<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
					<div class="ui-block-content">
						<div class="row">
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Tertanggung" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Nomor  SPAJ" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Pemegang Polis" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group is-empty">
									<input class="form-control" placeholder="Produk" type="text" required="">
								<span class="material-input"></span></div>
							</div>
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="crumina-module crumina-heading with-title-decoration">
									<h5 class="heading-title">Periode Tanggal UW</h5>
								</div>
							</div>	
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">Start Date</label>
									<input name="datetimepicker" value="10/24/1984" />
									<span class="input-group-addon">
										<svg class="olymp-month-calendar-icon icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
									</span>
								</div>
							</div>
							<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">End Date</label>
									<input name="datetimepicker" value="10/24/1984" />
									<span class="input-group-addon">
										<svg class="olymp-month-calendar-icon icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
									</span>
								</div>
							</div>

							<div class="col col-lg-4 col-md-4 col-sm-12 col-12">
								<div class="form-group label-floating is-select">
									<label class="control-label"></label>
									<select class="selectpicker form-control">
										<option value="All">-- Seluruhnya --</option>
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
										<option value="4">Option 4</option>
									</select>
								</div>
							</div>
							<div class="col col-lg-8 col-md-8 col-sm-12 col-12"></div>
							<div class="col col-lg-4 col-md-4 col-sm-12 col-12">
								<button class="btn btn-success btn-sm">Cari</button>
								<button class="btn btn-primary btn-sm">Reset</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- end filter -->

			<!-- data table -->
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="mb-0">
						Summary SPAJ Agen ANTONIO</h6>
				</div>
				<div class="ui-block-content">
					<div class="pull-right">
							<button class="btn btn-green btn-sm">Cetak</button>
							<button class="btn btn-blue btn-sm">Extract</button>
					</div>
					<table id="example" class="table table-striped table-bordered" >
							<thead>
								<tr>
									<th>Nama Agen</th>
									<th>Produk</th>
									<th data-toggle="tooltip" data-placement="top" data-original-title="Accepted">A</th>
									<th data-toggle="tooltip" data-placement="top" data-original-title="Declined">D</th>
									<th data-toggle="tooltip" data-placement="top" data-original-title="Pending">P</th>
									<th data-toggle="tooltip" data-placement="top" data-original-title="Unpaid">U</th>
									<th data-toggle="tooltip" data-placement="top" data-original-title="Cancelled">C</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Anton Sugriwa</td>
									<td>Produk example 1</td>
									<td>3</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>3</td>
								</tr>
								<tr>
									<td>Hani Uni</td>
									<td>Produk example 2</td>
									<td>4</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>4</td>
								</tr>
								<tr>
									<td>Bima Putra</td>
									<td>Produk example 3</td>
									<td>7</td>
									<td>1</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>8</td>
								</tr>
							</tbody>
						</table>
						<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">											
							<!-- Pagination -->							
							<nav aria-label="Page navigation">
								<ul class="pagination justify-content-center">
									<li class="page-item disabled">
										<a class="page-link" href="{{ url('/') }}/car_asset/#" tabindex="-1">Previous</a>
									</li>
									<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
									<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">2</a></li>
									<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">3</a></li>
									<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">...</a></li>
									<li class="page-item"><a class="page-link" href="{{ url('/') }}/car_asset/#">12</a></li>
									<li class="page-item">
										<a class="page-link" href="{{ url('/') }}/car_asset/#">Next</a>
									</li>
								</ul>
							</nav>
							<!-- ... end Pagination -->				
						</div>
		
					</div>
			</div>
			<!-- end data table -->
		</main>

		<!-- ... end Main Content -->


		<!-- Left Sidebar -->

		<aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
			<div class="ui-block">
				<!-- BAGIAN KIRI HALAMAN -->
				 <!-- 3 setup a container element -->
				<div class="ui-block-title">
					<h6 class="title">Your Achivement</h6>
					<a href="{{ url('/') }}/car_asset/#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ url('/') }}/car_asset/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>
				<div class="ui-block-content" id="jstree_demo_div">
					<!-- in this example the tree is populated from inline HTML -->
					<ul>
						<li data-jstree='{"icon":"fa fa-user "}'>888JWY - RITA
								<ul>
										<li data-jstree='{"icon":"fa fa-user "}'>888LOI - AMINULLAH</li>
										<li data-jstree='{"icon":"fa fa-user "}'>888LKJ - LEVERENCIS</li>
										<li data-jstree='{"icon":"fa fa-user "}'>564ASD - ANTONIO</li>
										</ul>
						</li>
						<li data-jstree='{"icon":"fa fa-user "}'>888LVB - JAK MENG</li>
						<li data-jstree='{"icon":"fa fa-user "}'>567OIS - SRI ASTUTI
								<ul>
										<li data-jstree='{"icon":"fa fa-user "}'>888LOI - TUKIJA</li>
										<li data-jstree='{"icon":"fa fa-user "}'>888LKJ - JUMIK</li>
										<li data-jstree='{"icon":"fa fa-user "}'>564ASD - RICHARDO</li>
										</ul>
						</li>
						<li data-jstree='{"icon":"fa fa-user "}'>989IOU - KONI</li>
						<li data-jstree='{"icon":"fa fa-user "}'>768KIJ - OLIVE</li>
					</ul>
				</div>
			</div>
		</aside>

		<!-- ... end Left Sidebar -->


		<!-- Right Sidebar -->

		<!-- <aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12"> -->
			<!-- BAGIAN KANAN HALAMAN -->
		<!-- </aside> -->

		<!-- ... end Right Sidebar -->

	</div>
</div>
@endsection

@section('page_script')

<script src="{{ url('/') }}/car_asset/js/base-init.js"></script>
<script defer src="{{ url('/') }}/car_asset/fonts/fontawesome-all.js"></script>

<script src="{{ url('/') }}/car_asset/Bootstrap/dist/js/bootstrap.bundle.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script>

	var $treeview = $(function () { $('#jstree_demo_div').jstree(); });
	
	$('#jstree_demo_div').on('loaded.jstree', function() {
		$('#jstree_demo_div').jstree('open_all');
	});

	$('#jstree_demo_div').on("changed.jstree", function (e, data) {
		console.log(data.selected);
	});

	$('button').on('click', function () {
	$('#jstree').jstree(true).select_node('child_node_1');
	$('#jstree').jstree('select_node', 'child_node_1');
	$.jstree.reference('#jstree').select_node('child_node_1');
	});

	$(document).ready(function() {
		$('#1').DataTable();
	} );
</script>
@endsection